var util = require('util');

try {
  throw new Error('Error!');
} catch (e) {
  console.log(e);
  console.log(util.inspect(e, true));
}
