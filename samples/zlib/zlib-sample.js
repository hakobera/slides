var zlib = require('zlib')
  , fs = require('fs');

var gzip = zlib.createGzip();
var inp = fs.createReadStream(__dirname + '/input.txt');
var out = fs.createWriteStream(__dirname + '/input.txt.gz');

inp.pipe(gzip).pipe(out);