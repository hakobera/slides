var connect=require('connect')
  , sio = require('socket.io');

var app = connect.createServer(
    connect.logger()
  , connect.static(__dirname + '/public')
);

app.listen(process.env.PORT || 3000);

// socket.io
var io = sio.listen(app);
io.set('log level', false);
io.set('transports', [process.env.LATENCY_TRANSPORT || 'websocket']);
io.sockets.on('connection', function (socket) {
  socket.on('message', function (msg) {
    socket.send(msg);
  });
});

process.on('uncaughtException', function(err) {
  // ignore
  console.error(err);
});